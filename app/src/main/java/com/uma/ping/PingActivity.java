/**
 * Developed by the University of Málaga
 *
 * Alberto Salmerón Moreno
 * Bruno García García
 * Gonzalo Chica Morales
 */

package com.uma.ping;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

public class PingActivity extends AppCompatActivity {
    private static String LOG = "ping." + PingActivity.class.getSimpleName();
    private static InterfaceIds gui;
    private LogReceiver logReceiver;
    private StatusReceiver statusReceiver;

    // Utility methods for sending broadcast messages to the application.
    public static void broadcast(LocalBroadcastManager manager, String message) {
        Intent logIntent = new Intent(PingService.LOG_BROADCAST_ACTION);
        logIntent.putExtra(PingService.LOG_STRING, message + "\n");
        manager.sendBroadcast(logIntent);
    }

    public static void notifyStatusChange(LocalBroadcastManager manager) {
        manager.sendBroadcast(new Intent(PingService.UPDATE_BROADCAST_ACTION));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ping);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorBlack));
        toolbar.setTitle(R.string.app_name);
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        gui = new InterfaceIds();
        gui.Log = (TextView) findViewById(R.id.logTextView);
        gui.Scroll = (ScrollView) findViewById(R.id.logScroll);
        gui.StartStopButton = (Button) findViewById(R.id.startStopButton);
        gui.TargetField = (TextView) findViewById(R.id.pingTarget);
        gui.TtlField = (TextView) findViewById(R.id.ttlValue);
        gui.IntervalField= (TextView) findViewById(R.id.intervalValue);
        gui.SizeField= (TextView) findViewById(R.id.sizeValue);

        if (savedInstanceState != null) { // Restore state
            gui.TargetField.setText(savedInstanceState.getString("Target"));
            gui.TtlField.setText(savedInstanceState.getString("Ttl"));
            gui.SizeField.setText(savedInstanceState.getString("Size"));
            gui.IntervalField.setText(savedInstanceState.getString("Interval"));
            gui.Log.setText(savedInstanceState.getString("Log"));
        } else { // Initialize

            boolean granted = checkPermissions();

            if (granted) {
                Log.d(LOG, "Permission granted");
            } else {
                gui.Log.append("No WRITE_EXTERNAL_STORAGE permission, cannot create log files.\n");
            }

            updateStatus();
            gui.TargetField.setText("www.google.com");
            gui.TtlField.setText("128");
            gui.SizeField.setText("56");
            gui.IntervalField.setText("1.0");
            gui.Log.append("Initialization completed.\n\n");
        }
    }

    private boolean checkPermissions() {
        Context context = getApplicationContext();
        String[] permissions = new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE };
        boolean passed = true;

        for (String permission : permissions) {
            passed = passed && checkOnePermission(context, permission);
        }

        if (!passed) {
            ActivityCompat.requestPermissions(this, permissions, 100);
        }

        return passed;
    }

    private boolean checkOnePermission(Context context, String permission) {
        boolean granted = ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        Log.d(LOG, permission + " granted: " + granted);
        return granted;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about_button:
                Log.d(LOG, "About");
                NoticeDialog about = NoticeDialog.newInstance();
                about.show(getSupportFragmentManager(), "AboutNotice");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("Target", gui.TargetField.getText().toString());
        savedInstanceState.putString("Ttl", gui.TtlField.getText().toString());
        savedInstanceState.putString("Interval", gui.IntervalField.getText().toString());
        savedInstanceState.putString("Size", gui.SizeField.getText().toString());
        savedInstanceState.putString("Log", gui.Log.getText().toString());
    }

    public void startStopButtonClicked(View view) {
        Log.i(LOG, "Start/Stop Button Clicked");
        switchStatus();
    }

    private void switchStatus() {
        boolean active = PingService.active();
        Intent intent = new Intent(this, PingService.class);

        if (active) {
            intent.setAction(PingService.STOP_ACTION);
        } else {
            intent.setAction(PingService.START_ACTION);

            String target = gui.TargetField.getText().toString();
            String ttl = gui.TtlField.getText().toString();
            String size = gui.SizeField.getText().toString();
            String interval = gui.IntervalField.getText().toString();

            String parameters = String.format("target=%1s,ttl=%2s,size=%3s,interval=%4s", target, ttl, size, interval);

            intent.putExtra(PingService.PARAMETERS_EXTRA, parameters);
        }
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        statusReceiver = new StatusReceiver();
        IntentFilter filter = new IntentFilter(PingService.UPDATE_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(statusReceiver, filter);

        logReceiver = new LogReceiver();
        filter = new IntentFilter(PingService.LOG_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(logReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(statusReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(logReceiver);
    }

    private void updateStatus() {
        if (PingService.active()) {
            gui.StartStopButton.setText("Stop");
        } else {
            gui.StartStopButton.setText("Start");
        }
        gui.TtlField.setText(PingService.lastTtl());
        gui.TargetField.setText(PingService.lastTarget());
    }

    private class InterfaceIds {
        public TextView Log;
        public ScrollView Scroll;
        public Button StartStopButton;
        public TextView TargetField;
        public TextView TtlField;
        public TextView IntervalField;
        public TextView SizeField;
    }

    private class StatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateStatus();
        }
    }

    private class LogReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String message = intent.getStringExtra(PingService.LOG_STRING);
            if (message != null) {
                gui.Log.append(message);
                gui.Scroll.post(new Runnable() {
                    public void run() {
                        gui.Scroll.smoothScrollTo(0, gui.Log.getBottom());
                    }
                });
            }
        }
    }
}
