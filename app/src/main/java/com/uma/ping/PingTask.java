/**
 * Developed by the University of Málaga
 *
 * Alberto Salmerón Moreno
 * Bruno García García
 * Gonzalo Chica Morales
 */

package com.uma.ping;

import android.content.Context;
import android.os.AsyncTask;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bruno on 09/06/2016.
 */
public class PingTask extends AsyncTask<String, String, Integer> {
    private static String LOG = "ping." + PingTask.class.getSimpleName();
    private static String MESSAGE = "ping.Message";
    private static String REPORT = "ping.Report";

    private String ping;
    private String taskId;
    private Context context;

    private int time = 0;
    private Pattern pattern;
    private OutputStreamWriter file;
    private SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");

    public PingTask(Context c, int id) {
        context = c;
        taskId = "Ping<" + id + ">";
        ping = "/system/bin/ping";
        pattern = Pattern.compile(".*time=(\\d+(.\\d)?) *ms.*");
        fileOpen();
    }

    private void fileOpen() {
        String folderpath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/pingAgent/";
        String filepath = folderpath + format.format(Calendar.getInstance().getTime()) + ".txt";

        try {
            File folderHandle = new File(folderpath);
            if (!folderHandle.exists()) {
                Log.i(LOG, "Creating app folder. Success: " + folderHandle.mkdirs());
            }

            File fileHandle = new File(filepath);
            if (!fileHandle.createNewFile()) {
                throw new IOException("File " + filepath + " already exists");
            }

            file = new OutputStreamWriter(new FileOutputStream(fileHandle));
        }
        catch (IOException e) {
            Log.e(LOG, "File open '" + filepath + "' failed: " + e.toString());
        }
    }

    private void fileClose() {
        if (file != null) {
            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                Log.e(LOG, "File close failed: " + e.toString());
            }
        }
    }

    protected Integer doInBackground(String... params) {
        Process process;
        List<String> commandList = new ArrayList<>();
        commandList.add(ping);
        commandList.addAll(Arrays.asList(params));

        Log.i(LOG, String.format("Starting Ping task <%1s>.\n" +
                " Command:", taskId) + commandList.toString());

        NonBlockingReader reader;
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
        time = 0;

        try {
            process = new ProcessBuilder().command(commandList).start();
            reader = new NonBlockingReader(new InputStreamReader(process.getInputStream()));
        } catch (IOException e) {
            String msg = "Error while launching Ping process: " + e.getMessage();
            PingActivity.broadcast(broadcastManager, msg);
            Log.i(MESSAGE, "<<<Error:" + msg + ">>>");
            Log.e(LOG, msg);
            fileClose();
            return -1;
        }

        while (!isCancelled() && !reader.finished()) {
            if (reader.ready()) {
                updateLogs(reader.read());
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.d(LOG, "Interrupted while waiting for Ping process output: " + e.getMessage());
            }
        }
        reader.close();

        // If the process has not finished, accessing the exitValue will
        // throw an exception. In this case we force-close the process.
        try {
            int value = process.exitValue();
            Log.i(LOG, "Ping return value is: " + value);

            if (value > 1) { // Error codes start at 2 (0: No error, 1: No reply)
                PingActivity.broadcast(broadcastManager, "Ping failed with error code " + value + ":");

                String line;
                BufferedReader errorReader = null;
                try {
                    errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                    while ((line = errorReader.readLine()) != null) {
                        PingActivity.broadcast(broadcastManager, line);
                        Log.i(MESSAGE, "<<<Error:" + line + ">>>");
                    }
                    errorReader.close();
                } catch (IOException e) {
                    Log.e(LOG, "Exception found while reading Ping error output: " + e.getMessage());
                } finally {
                    try {
                        if (errorReader != null) {
                            errorReader.close();
                        }
                    } catch (IOException e) {
                        Log.e(LOG, "Exception while closing error stream reader: " + e.getMessage());
                    }
                }
            }
        } catch (IllegalThreadStateException e) {
            process.destroy();
            Log.i(LOG, "Ping process has been terminated.");
        }

        PingActivity.broadcast(broadcastManager, "Ping task has finished.");
        PingService.finished();

        PingActivity.notifyStatusChange(broadcastManager);
        fileClose();
        return 0;
    }

    private void updateLogs(String message) {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);

        // Required to ensure that each logCat line corresponds to a single line of Ping output
        String[] lines = message.split("\\r\\n|\\n|\\r");
        for (int l = 0; l < lines.length; l++) {
            if (!lines[l].isEmpty()) {
                long timestamp = System.currentTimeMillis();
                Log.i(LOG, "<<< Timestamp: " + timestamp + " ; Output: " + lines[l].trim() + " >>>");
                String report = null;

                double delay;
                Matcher match = pattern.matcher(lines[l]);
                if (match.find()) {
                    try {
                        delay = Double.parseDouble(match.group(1));
                        PingActivity.broadcast(broadcastManager, "Delay: " + delay + " ms");
                        report = "<<< Timestamp: " + timestamp + " ; " + "Time:" + time + " ; Delay:" + delay + " >>>";
                        time++;
                    } catch (NumberFormatException e) {
                        Log.e(LOG, "Matched value is not a valid double: " + match.group(1));
                    }
                } else if (lines[l].contains("no answer yet for icmp_seq")) {
                    delay = Double.MAX_VALUE;
                    PingActivity.broadcast(broadcastManager, "Ping request timed out.");
                    report = "<<< Timestamp: " + timestamp + " ; " + "Time:" + time + " ; Delay:" + delay + " >>>";
                    time++;
                }

                if (report != null) {
                    Log.i(REPORT, report);
                    try {
                        file.write(report + '\n');
                    } catch (IOException e) {
                        Log.e(LOG, "Could not write to file: " + e.toString());
                    }
                }
            }
        }
    }
}
