/**
 * Developed by the University of Málaga
 *
 * Alberto Salmerón Moreno
 * Bruno García García
 * Gonzalo Chica Morales
 */

package com.uma.ping;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;

public class PingService extends Service {
    public final static String START_ACTION = "com.uma.ping.START";
    public final static String STOP_ACTION = "com.uma.ping.STOP";
    public final static String PARAMETERS_EXTRA = "com.uma.ping.PARAMETERS";
    public final static String UPDATE_BROADCAST_ACTION = "com.uma.ping.UPDATEACTION";
    public final static String LOG_BROADCAST_ACTION = "com.uma.ping.LOGACTION";
    public final static String LOG_STRING = "com.uma.ping.LOGSTRING";

    private static String LOG = "ping." + PingService.class.getSimpleName();
    private static String MESSAGE = "ping.Message";
    private static PingTask pingTask = null;
    private static String lastTarget = "";
    private static String lastTtl = "128";

    private static int taskId = 0;

    public PingService() {
    }

    public static void finished() {
        Log.i(LOG, "Received notification that ping has finished.");
        pingTask = null;
    }

    public static boolean active() {
        return pingTask != null;
    }

    public static String lastTarget() {
        return lastTarget;
    }

    public static String lastTtl() {
        return lastTtl;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        taskId++;

        if (action == null) {
            Log.w(LOG, "Received intent with no action. Call ignored.");
        } else {
            String extras = intent.getStringExtra(PARAMETERS_EXTRA);
            String[] params = new String[0];
            String cmd = "";
            String target, ttl, size, interval;
            target = ttl = size = interval = "";
            if (extras != null) {
                Log.i(LOG, "Parameters: " + extras);
                params = extras.split(",");
                    int check = 0;
                    for (int i = 0; i < params.length; i++){
                        String[] parts = params[i].split("=");
                        if (parts.length == 2){
                            switch (parts[0]){
                                case "target": target = parts[1]; check += 1; break;
                                case "ttl": ttl = parts[1]; check += 2; break;
                                case "size": size = parts[1]; check += 4; break;
                                case "interval": interval = parts[1]; check += 8; break;
                            }
                        }
                    }
                    if (check == 15){
                        // The parameters used are:
                        // -n   (Do not to lookup symbolic names for host addresses)
                        // -O   (Report outstanding ICMP ECHO reply before sending  next  packet.)
                        // -t ? (Configured TTL)
                        // -i ? (Configured Interval)
                        // -s ? (Configured Size)
                        cmd = String.format("-n,-O,-t,%1s,-i,%2s,-s,%3s,%4s", ttl, interval, size, target);
                    }
            }

            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
            String output = "Error while processing received service request.";

            switch (action) {
                case START_ACTION:
                    Log.i(LOG, "START_ACTION");
                    if (pingTask != null) {
                        output = "Ping is already started. Call ignored";
                        Log.i(MESSAGE, "<<<Warning:" + output + ">>>");
                        Log.e(LOG, output);
                    } else {
                        if (cmd.length() == 0){
                            output = "Not all ping parameters have been configured. Call ignored.";
                            Log.i(MESSAGE, "<<<Error:" + output + ">>>");
                        } else {
                            pingTask = new PingTask(this, taskId);
                            pingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, cmd.split(","));
                            output = String.format("Ping started: Target:%1s, TTL:%2s, Size:%3s, Interval:%4s", target, ttl, size, interval);
                            lastTarget = target;
                            lastTtl = ttl;
                            PingActivity.notifyStatusChange(broadcastManager);
                        }
                    }
                    break;
                case STOP_ACTION:
                    Log.i(LOG, "STOP_ACTION");
                    if (pingTask == null) {
                        output = "Ping task is not running. Call ignored";
                        Log.i(MESSAGE, "<<<Warning:" + output + ">>>");
                        Log.e(LOG, output);
                    } else {
                        pingTask.cancel(true);
                        pingTask = null;
                        output = "Stopping ping.";
                        PingActivity.notifyStatusChange(broadcastManager);
                    }
                    break;
            }
            PingActivity.broadcast(broadcastManager, output);
        }
        return Service.START_NOT_STICKY;
    }
}
